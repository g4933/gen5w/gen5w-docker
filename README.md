# gen5w-docker

## Running

To build you just need to replace the dummy `mango-rootfs.tar.gz` with the one from your car's update package and build or you can have mango-rootfs already extracted on a folder named `mango-rootfs`

Changes done on the container on the `/mango` directory will persist outside of the container on the folder `mango-rootfs` on the host.

The following commands must be used to build (or the mount for the CHROOT fails)
```sh
docker compose build
docker compose run mango # If you just want to enter the docker image
docker compose run mango /chroot.sh # If you want to enter with chroot mode
```

## What can I do?
You can modify mango-rootfs from outside the container and test the changes within...

## Project Status: Pre-Alpha

This project in it's current form is just a way to "run" binaries from your car in the container. Most of the time these binaries will fail but this container now has all the libraries mapped to allow the applications *to* run.

Currently functionality missing/needing mocks:

- No way to pipe Qt apps out for viewing
- No hardware has been mocked out for the apps to connect to (i.e. various fw updaters fail on inexistent hardware)
- Navigation Software has not been addressed at all (No nav apps have had their libraries resolved, the main nav app is not included yet, etc.)
