#!/bin/bash

mkdir -p /mango /tmp/libtse

[ "$(ls /mango)" ] && echo "./mango-rootfs folder seems populated, we won't untar!" || (echo "Wait until we extract mango-rootfs.tar.gz onto ./mango-rootfs" && pv /mango-rootfs.tar.gz | tar -xzf - -C /mango/)

ln -s /mango/app /app
ln -s /mango/usr/lib/app /app/lib
ln -s /mango/lib/libcrypto.so* /app/lib

ln -s /lib/x86_64-linux-gnu/ld-2.31.so /lib/ld-linux-x86-64.so.2 
ln -s /app/bin/vbtd-51 /tmp/vbtd 
ln -s /app/lib/libtse45.so /tmp/libtse/libtse.so 
ln -s /usr/lib/x86_64-linux-gnu/libEGL.so.1.1.0 /usr/lib/libEGL.so.1

ldconfig

/bin/bash