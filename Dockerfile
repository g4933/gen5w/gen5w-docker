ARG PLATFORM="linux/x86_64"

# # Build libpersistence_client_library.so.7.1.1 so we don't need to copy it from the update file
# FROM --platform=${PLATFORM} ubuntu:20.04 as builder
# RUN curl -o libpersistence_client_library_1.2.1.tar.gz https://github.com/GENIVI/persistence-client-library/archive/refs/tags/v1.2.1.tar.gz
# RUN tar xzvf libpersistence_client_library_1.2.1.tar.gz
# RUN make
# RUN apt update && apt install -y software-properties-common

FROM --platform=${PLATFORM} ubuntu:20.04

# First we install the dependencies. This is done so that in case the tar.gz is changed or anything below, we don't loose this layer if it was already built.
RUN DEBIAN_FRONTEND=noninteractive apt update && apt install -yq \
    libasound2 libcgroup1 libdbus-glib-1-2 libgstreamer1.0-0 libid3tag0 \ 
    libjsoncpp1 libmediainfo0v5 libphonenumber7 libqt5core5a libqt5concurrent5 \
    libqt5sql5 libqt5dbus5 libqt5network5 libqt5qml5 libqt5gui5 libqt5widgets5 libqt5opengl5 libqt5xml5 \
    libqt5quick5 libqt5multimedia5 libsndfile1 libusb-1.0-0 liborc-0.4-0 libebml4v5 libflac8 \
    libgdk-pixbuf2.0-0 libgstreamer-plugins-base1.0-0 liblz4-1 libmagic1 libogg0 libmatroska6v5 \
    libpng16-16 libtag1v5 libtheora0 libvorbis0a libjpeg9 libreadline5 pv vim binutils

COPY ./chroot.sh /chroot.sh
COPY ./entry.sh /entry.sh
RUN chmod +x /chroot.sh && chmod +x /entry.sh
CMD ["/entry.sh"]