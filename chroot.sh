#!/bin/bash

mkdir -p /mango/ /mango/vr /mango/log_data /mango/update /mango/voicememo /mango/rw_data /mango/Data/mnt-c /mango/Data/mnt-wt /mango/Data mnt-backup /mango/navi /mango/navi2 /mango/space /mango/proc /mango/sys /mango/dev /mango/dev/shm /mango/dev/pts /mango/wslg

mount -t proc proc /mango/proc
mount -t sysfs sysfs /mango/sys
mount -t devtmpfs devtmpfs /mango/dev
mount -t tmpfs tmpfs /mango/dev/shm
mount -t tmpfs /mnt/wslg /mango/wslg
mount -t devpts devpts /mango/dev/pts

# Copy /etc/hosts
/bin/cp -f /etc/hosts /mango/etc/

# Copy /etc/resolv.conf 
/bin/cp -f /etc/resolv.conf /mango/etc/resolv.conf

printf "/app/lib\n/app/lib/lightmediascanner/plugins/\n/tmp/\n/tmp/libtse" >> /mango/etc/ld.so.conf

chroot /mango rm /etc/mtab 2> /dev/null 
chroot /mango ln -s /proc/mounts /etc/mtab

chroot /mango bash